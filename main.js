import "./style.scss"

window.addEventListener('load', () => {
    const user = localStorage.getItem('user')
    const pass = localStorage.getItem('pass')
    const header = document.querySelector(".header")
    const whoWeAre = document.querySelector(".whoWeAre")
    const assortment = document.querySelector(".assortment")
    const cost = document.querySelector(".cost")
    const contact = document.querySelector(".contact")
    const menuBtn = document.querySelector('.menu')
    const menu = document.querySelector('.sideMenu .list')
    let page = ""
    let menuVisible = false

    if (user && pass) {

    } else {
        const menu = document.createElement("div")
        menu.className = "menu"

        const buttonSignIn = document.createElement("a")
        const buttonSignUp = document.createElement("a")

        buttonSignIn.classList = "button signIn"
        buttonSignUp.classList = "button signUp"

        buttonSignUp.innerText = "Sign Up"
        buttonSignIn.innerText = "Sign In"

        buttonSignUp.href = "/"
        buttonSignIn.href = "/"

        menu.appendChild(buttonSignIn)
        menu.appendChild(buttonSignUp)

        header.appendChild(menu)
    }

    const scrollTo = (element, c) => {
        if (!element.classList.contains("active")) {
            window.scroll({
                left: 0,
                top: element.offsetTop - (c * element.offsetHeight/5),
                behavior: "smooth"
            })
        }
    }

    const seeMore = document.querySelector(".seeMore")

    seeMore.addEventListener('click', () => scrollTo(whoWeAre, 0))

    document.querySelectorAll(".listElement")?.forEach((e, i) => {
        switch (i) {
            case 0:
                e.addEventListener('click', () => scrollTo(whoWeAre, 0))
                break;
            case 1:
                e.addEventListener('click', () => scrollTo(assortment, 0))
                break;
            case 2:
                e.addEventListener('click', () => scrollTo(cost, 2))
                break;
            case 3:
                e.addEventListener('click', () => scrollTo(contact,0))
                break;
        }

    })

    const setPage = (name) => {
        document.querySelectorAll(".listElement")?.forEach(e => {
            e.classList.remove("active")
        })
        document.querySelector(`.sideMenu [data-element=${name}]`).classList.add("active")
    }

    const setMenu = () => {
        let posTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;

        if (posTop < (window.innerHeight + whoWeAre.offsetHeight - assortment.offsetHeight / 3)) {
            if (page !== "whoWeAre") {
                page = "whoWeAre"
                setPage(page)
                document.querySelector(".sideMenu").classList.remove("bottom")
            }
        } else if (posTop < (window.innerHeight + whoWeAre.offsetHeight + assortment.offsetHeight - cost.offsetHeight / 3)) {
            if (page !== "assortment") {
                page = "assortment"
                setPage(page)
                document.querySelector(".sideMenu").classList.remove("bottom")
            }
        } else if (posTop < (window.innerHeight + whoWeAre.offsetHeight + assortment.offsetHeight - contact.offsetHeight/3.5)) {
            if (page !== "cost") {
                page = "cost"
                setPage(page)
                document.querySelector(".sideMenu").classList.remove("bottom")
            }
        } else if (posTop < (window.innerHeight + whoWeAre.offsetHeight + assortment.offsetHeight + cost.offsetHeight + contact.offsetHeight)) {
            if (page !== "contact") {
                page = "contact"
                setPage(page)
               document.querySelector(".sideMenu").classList.add("bottom")
            }
        }
    }

    window.addEventListener('scroll', setMenu)

    setMenu()

    menuBtn.addEventListener("click", () => {
        const img = document.querySelector('.menu img')
        menuVisible = !menuVisible
        if (menuVisible) {
            menu.classList.add("open")
            menuBtn.classList.add("clicked")
            img.src = "/closed.svg"
        } else {
            menu.classList.remove("open")
            menuBtn.classList.remove("clicked")
            img.src = "/menu.svg"
        }
    })
    $(".assortment .slider").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: false,
        leftPadding: '40px',
        dots: true,
        arrows: false,
        responsive: [
            {
                breakpoint: 1040,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    })
    $(".cost .slider").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        arrows: false,
        variableWidth: true,
        centerMode: true,
        speed: 300,
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
            {
                breakpoint: 1040,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    })
    document.querySelectorAll(".card").forEach((e) => {
        e.addEventListener('mouseup', () => {

            let name = e.querySelector("h5").innerText
            let description = e.querySelector("p").innerText
            const prev = e.querySelector("img").src
            const cost =  e.querySelector("h4").innerText
            document.querySelector(".cover")?.remove()

            const cover = document.createElement("div")
            cover.classList.add('cover')
            cover.addEventListener('click', () => {
                cover.classList.add("finish")
                setTimeout(() => {
                    document.querySelector(".cover")?.remove()
                }, 300)
            })

            const card = document.createElement("div")
            card.classList.add("card")
            card.classList.add("card-background")
            card.dataset.number = e.dataset.number
            card.addEventListener('click', (event)=>{
                event.stopPropagation()
                event.preventDefault()
            })

            const h5 = document.createElement("h5")
            h5.innerText = name
            card.appendChild(h5)



            const content = document.createElement("div")
            content.classList.add("content")


            const img = document.createElement("img")
            img.src = prev
            img.alt = ""
            content.appendChild(img)

            const menu = document.createElement("div")
            menu.classList.add("menu")

            const p = document.createElement("p")
            p.innerText = description
            menu.appendChild(p)


            const price = document.createElement("h4")
            price.innerText = cost
            menu.appendChild(price)

            const button = document.createElement("button")
            button.innerText = "Детальніше..."
            menu.appendChild(button)

            content.appendChild(menu)
            card.appendChild(content)

            const closed = document.createElement("div")
            closed.classList.add("closed")

            const closedImg = document.createElement("img")
            closedImg.src = "/closed.svg"
            closed.appendChild(closedImg)
            closed.addEventListener('click', ()=>{
                cover.classList.add("finish")
                setTimeout(() => {
                    document.querySelector(".cover")?.remove()
                }, 299)
            })

            card.appendChild(closed)
            cover.appendChild(card)
            document.body.appendChild(cover)
        })
    })
})
